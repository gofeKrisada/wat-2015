﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PathBorder : MonoBehaviour {

	public RectTransform textRect;

	#region Trigger 
	
	void OnTriggerEnter(Collider obj){
		
		if(obj.CompareTag("Player")){
			LeanTween.textColor(textRect.GetChild(UserData.lang-1).GetComponent<RectTransform>(),new Color(1,1,1,1),0.35f);		
			StartCoroutine(FadeText(2));
		}//tag talk
		
	}//trigger Enter
	

	
	#endregion


	IEnumerator FadeText(float time) {

		yield return new WaitForSeconds(time);
		LeanTween.textColor(textRect.GetChild(UserData.lang-1).GetComponent<RectTransform>(),new Color(1,1,1,0),3f);	
	}//disColl

}//class
