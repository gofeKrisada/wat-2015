﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerControl : MonoBehaviour {




	private Camera mainCam;
	private RectTransform blackBG;
	public GameObject canControl;
	// Use this for initialization
	void Start () {
		mainCam = Camera.main;

			blackBG = GameObject.FindWithTag("CanvasTop").transform.GetChild(0).GetComponent<RectTransform>();

		canControl = transform.GetChild(2).GetChild(0).gameObject;


		dialogPort = dialogBox.GetChild(0).GetComponent<Image>();
		nameText = dialogBox.GetChild(2).GetComponent<Text>();
		dialogText  =dialogBox.GetChild(3).GetComponent<Text>();

		dialogOk =dialogBox.GetChild(4);


	}//start



	public GameObject talk_button;
	public Transform dialogBox;
	private Text dialogText; 
	private Text nameText; 
	private Transform dialogOk;

	private  int dialogCount;
	private int dialogCurrent;

	private Image dialogPort;

	private NpcDialog npcDialog;



	public void ShowDialog(){
		AudioPlayer.Play(AudioPlayer.sfx_button1);

		canControl.SetActive(false);
		Camera.main.GetComponent<MouseLook>().enabled=false;
		this.GetComponent<MouseLook>().enabled=false;

		StartCoroutine(SwitchCamNPC(0.5f));

		


	}// show dialog

	public void DialogOK(){
		AudioPlayer.Play(AudioPlayer.sfx_DialogOk);
		dialogCount-=1;
		dialogCurrent+=1;
			if(dialogCount>0){
				// playSFX

				dialogText.text =  npcDialog.dialogSet[dialogCurrent];
			StartCoroutine(DisableOK());
		}//more conversation
		else {
	
			dialogCount=0;
			dialogCurrent=0;
			dialogBox.gameObject.SetActive(false);




			StartCoroutine(SwitchCamMain(0.5f));
		}//close Dialog
	}//dialogOK

	IEnumerator DisableOK() {
		dialogOk.gameObject.SetActive(false);
		yield return new WaitForSeconds(0.25f);
		dialogOk.gameObject.SetActive(true);
	}//disColl

	IEnumerator SwitchCamNPC(float time) {
		blackBG.gameObject.SetActive(true);
		LeanTween.alpha(blackBG,1,time);
		yield return new WaitForSeconds(time);

		mainCam.GetComponent<Camera>().enabled =false;
		talkNPC.GetChild(3).GetComponent<Billboard>().cam = talkNPC.GetChild(4).GetComponent<Camera>();
		talkNPC.GetChild(4).GetComponent<Camera>().enabled=true;
		LeanTween.alpha(blackBG,0,time);
		//----------------------------------------------------------------
		dialogBox.gameObject.SetActive(true);
		talk_button.SetActive(false);
		
		
		npcDialog = talkNPC.GetComponent<NpcDialog>();
		
		nameText.text =npcDialog.nameSet ;
		
		dialogCount = (int)npcDialog.dialogSet.Length;
		dialogText.text =   npcDialog.dialogSet[0];
		
		dialogPort.sprite = npcDialog.protrait;



	}//disColl

	IEnumerator SwitchCamMain(float time) {
		LeanTween.alpha(blackBG,1,time);
		yield return new WaitForSeconds(time);

		if(isTriggerEnter)	talk_button.SetActive(true);
		talkNPC.GetChild(4).GetComponent<Camera>().enabled=false;
		mainCam.GetComponent<Camera>().enabled  =true;
		talkNPC.GetChild(3).GetComponent<Billboard>().cam = Camera.main;
		LeanTween.alpha(blackBG,0,time);


		canControl.SetActive(true);
		if(isTriggerEnter){	Camera.main.GetComponent<MouseLook>().enabled=true;
		this.GetComponent<MouseLook>().enabled=true;
		}

		StartCoroutine(DisableBlack(time));
	}//disColl

	IEnumerator DisableBlack(float time) {

		yield return new WaitForSeconds(time);
		blackBG.gameObject.SetActive(false);
	}//disColl
	

	#region Trigger 

	public Transform talkNPC;

	public int tutTalk;

	private bool isTriggerEnter;
	void OnTriggerEnter(Collider obj){

		if(obj.CompareTag("TalkNPC")){
			AudioPlayer.Play(AudioPlayer.sfx_buttonPop);
				//appear something 
			talk_button.SetActive(true);
			talkNPC = obj.transform.parent;
			isTriggerEnter=true;
		}//tag talk


	else 	if(obj.name=="HotSpot_Collider"){
			//appear something 
			tutTalk=1;
		}//tut hotspot


	}//trigger Enter

	void OnTriggerExit(Collider obj){
		
		if(obj.CompareTag("TalkNPC")){
			isTriggerEnter=false;
				//disappear
			talk_button.SetActive(false);
	
		}//tag talk
		
	}//trigger Exit

	#endregion

}//class
