﻿using UnityEngine;
using System.Collections;

public class AudioPlayer : MonoBehaviour {



	public static Transform T;
	// Use this for initialization
	void Start () {
		T=this.transform;

		sfx_DialogOk = Resources.Load("Audio/sfx_DialogOk") as AudioClip;
		sfx_button1 = Resources.Load("Audio/sfx_button1") as AudioClip;
		sfx_buttonPop = Resources.Load("Audio/sfx_buttonPop") as AudioClip;
		
	}//start


	public static  int sfxPlayCount;

	public static int sfxCount;
	
	public static void Play(AudioClip clip){
		
		T.GetChild(sfxCount).GetComponent<AudioSource>().PlayOneShot(clip);
		
		sfxCount+=1;
		if(sfxCount>=2)sfxCount=0;
	}//play


	#region Audio List

	public static AudioClip sfx_DialogOk;
	public static AudioClip sfx_button1;
	public static AudioClip sfx_buttonPop;


	#endregion


}//class
