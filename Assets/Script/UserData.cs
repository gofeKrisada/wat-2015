﻿using UnityEngine;
using System.Collections;

public class UserData : MonoBehaviour {


	public static int lang=1;

	public static int lastScene; 

	public static int spawnDestination;

	
	public static int isTut
	{
		get
		{
			if(PlayerPrefs.HasKey("isTut"))
			{
				return (PlayerPrefs.GetInt("isTut"));
			}else{
				PlayerPrefs.SetInt("isTut", 0);
				PlayerPrefs.Save();
				return (PlayerPrefs.GetInt("isTut"));
			}
		}
		set
		{
			PlayerPrefs.SetInt("isTut", value);
			PlayerPrefs.Save();
		}
	}//star
	
}//class
