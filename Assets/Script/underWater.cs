using UnityEngine;
using System.Collections;

public class underWater : MonoBehaviour {

    public Vector2 speedMainTex;
	Vector2 offsetMainTex = Vector2.zero;
	
	//public Vector2 speedBump;    
	//Vector2 offsetBump = Vector2.zero;
	
	
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {

	       offsetMainTex += speedMainTex * Time.deltaTime;
	       if (offsetMainTex.x >= 1 || offsetMainTex.x <= -1)
	       {
	           offsetMainTex.x = 0;
	       }
	       if (offsetMainTex.y >= 1 || offsetMainTex.y <= -1)
	       {
	           offsetMainTex.y = 0;
	       }
	       GetComponent<Renderer>().materials[0].SetTextureOffset("_MainTex", offsetMainTex);
		
		
		
		
		/*
			offsetBump += speedBump * Time.deltaTime;
	       if (offsetBump.x >= 1 || offsetBump.x <= -1)
	       {
	           offsetBump.x = 0;
	       }
	       if (offsetBump.y >= 1 || offsetBump.y <= -1)
	       {
	           offsetBump.y = 0;
	       }
	       renderer.materials[0].SetTextureOffset("_BumpMap", offsetBump);*/
	}
}
