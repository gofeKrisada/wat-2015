﻿using UnityEngine;
using System.Collections;

public class MoveControl : MonoBehaviour {



	private int lane; 

	private Transform navMaster;
	private NavMeshAgent navA;
	private int moveDir ;
	private int currentDes;


	private Vector3 tempDes;
	// Use this for initialization



	void Start () {

		lane = Random.Range(0,3);


		StartCoroutine(DisableCollider(0.25f));

		navMaster = GameObject.FindWithTag("NavMaster").transform;

		navA = this.GetComponent<NavMeshAgent>();


		SetDes();

	
	}//start
	




	void OnTriggerEnter(Collider obj){

			if(obj.CompareTag("NavPoint")){
			if(obj.name=="TP_Set (0)"|| obj.name=="TP_Set (6)"){
				StartCoroutine(DisableCollider(4));
				StartCoroutine(DisableAgent(0.5f));
				ResetMove();
			}// S to N
			else{
				if(currentDes>0 && currentDes <6 ){
						navA.destination = navMaster.GetChild(currentDes+moveDir).GetChild(lane).position;
				currentDes +=moveDir;
							}
			}
		}// hit np




	}//onTriggerEnter

	void SetDes(){
		// ---- find move dir -----
		if(transform.eulerAngles.y >=0 && transform.eulerAngles.y <=180){
			
			moveDir=1;
			for(int i=0;i<7;i++){
				if(  transform.position.x>=navMaster.GetChild(i).position.x ){
					tempDes = navMaster.GetChild(i+1).position ;
					currentDes=i+1;
					
				}
				
			}//find current
				
		}
		else {
			
			moveDir=-1;
			for(int i=6;i>-1;i--){
				
				if(transform.position.x<=navMaster.GetChild(i).position.x ){
					tempDes = navMaster.GetChild(i-1).position ;
					currentDes=i-1;
					
				}
			}//find current
		}
		// ---- find move dir -----


		navA.destination = tempDes;
	}//setDes

	private int rand;
	void ResetMove(){
		rand = Random.Range(0,2);
		if(rand==1)rand=6;



		if(rand==0)moveDir=1;
		else moveDir=-1;

	
		lane = Random.Range(0,3);

		transform.position = navMaster.GetChild(rand).GetChild(lane).position;
		transform.rotation = navMaster.GetChild(rand).GetChild(lane).rotation;

		currentDes = rand+moveDir;

		//Switch Char

	}//resetMove


	IEnumerator DisableCollider(float time) {
		this.GetComponent<Collider>().enabled=false;
		yield return new WaitForSeconds(time);
		this.GetComponent<Collider>().enabled=true;
	}//disColl

	IEnumerator DisableAgent(float time) {
		this.GetComponent<NavMeshAgent>().enabled=false;
		yield return new WaitForSeconds(time);
		this.GetComponent<NavMeshAgent>().enabled=true;
		navA.destination = navMaster.GetChild(rand+moveDir).GetChild(lane).position;
	}//disColl



}//class
