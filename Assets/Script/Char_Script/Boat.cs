﻿using UnityEngine;
using System.Collections;

public class Boat : MonoBehaviour {

	void Start(){
		for(int i =0;i<transform.childCount;i++){
			LeanTween.moveLocalY(transform.GetChild(i).gameObject,0.01f,0.75f).setEase(LeanTweenType.easeInOutSine).setLoopPingPong();
		}//for

		loadingSet =   GameObject.FindWithTag("CanvasTop").transform.GetChild(1).gameObject;

		//DontDestroyOnLoad(loadingSet.transform.parent.gameObject);
	}//

	// Use this for initialization
	void Update() {	
			/*		
		if(Input.GetKeyUp(KeyCode.Escape))  	{
	
			AudioPlayer.Play(AudioPlayer.sfx_button1);
			StartCoroutine(DisableLoading(2));
		}*/


		for(int i =0;i<transform.childCount;i++){
		transform.GetChild(i).Translate(Vector3.right*Time.deltaTime*0.35f);

			if(transform.GetChild(i).localPosition.x >-900)
				transform.GetChild(i).localPosition = new Vector3(-1060,	transform.GetChild(i).localPosition.y,	transform.GetChild(i).localPosition.z);
		}//for
	}//start

	private GameObject loadingSet;
	
	private IEnumerator LoadAsync(string levelName)
	{
		

		AsyncOperation operation = Application.LoadLevelAsync(levelName);
		
		while(!operation.isDone) {
			yield return operation.isDone;
		//	Debug.Log("loading progress: " + operation.progress);
		}
	
	//	Debug.Log("load done");
	}//loadAsync 


	IEnumerator DisableLoading(float time) {
		//
		loadingSet.SetActive(true);
		LeanTween.rotateAround(loadingSet.transform.GetChild(1).GetComponent<RectTransform>(),Vector3.forward,360,6).setEase(LeanTweenType.linear).setLoopClamp();
		yield return new WaitForSeconds(time);
		StartCoroutine(LoadAsync("Scene_Menu"));
	}//disLoading

	public void BackMenu(){
		AudioPlayer.Play(AudioPlayer.sfx_button1);
		StartCoroutine(DisableLoading(2));
	}//back menu


}//class
