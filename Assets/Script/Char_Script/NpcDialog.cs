﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NpcDialog : MonoBehaviour {

	public Sprite protrait;

	public string[] dialogSet ;
	public string nameSet;

	public string nameEN;
	public string nameCN;

	public string[] dialogEN;
	public string[] dialogCN;





	void Start(){
		dialogSet  = new string[dialogEN.Length];


		if(UserData.lang == 1){
			dialogSet=dialogEN;
			nameSet=nameEN;
		}//en
		else if(UserData.lang == 2){
			dialogSet=dialogCN;
			nameSet=nameCN;
		}//cn



	}//start


}//class
