﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NpcGenerator : MonoBehaviour {

	public Transform[] nationPrefab;

	public Transform dynamicMaster;

	public  Transform Nav_Master;
	public Transform dChar;

	public int charCount;

	public Transform tempDes;

	private NavMeshAgent navA;

	Quaternion rot = Quaternion.identity;
	// Use this for initialization
	void Start () {
		TypeQuery();

		navA = this.GetComponent<NavMeshAgent>();


		navA.destination = Nav_Master.GetChild(currentDes+1).GetChild(0).position;

		interval = 6 /(float)charCount;
	}//start

	private float interval;
	private float tTime;
	private int amount;

	private GameObject tTemp;
	private GameObject tTemp2;
	private int rand;

	private float randS;

	void Update(){
		tTime+=Time.deltaTime;

		if(tTime > interval * amount && amount <charCount ){
			rand=Random.Range(0,indexList.Count);
					//print (indexList[rand]);
	

			amount+=1;
			rot .eulerAngles = new Vector3(0,Random.Range(0,359),0);
			tTemp = (GameObject)Instantiate(dChar.gameObject, this.transform.position,rot) ;
			tTemp.transform.parent = dynamicMaster;

			tTemp2 = (GameObject)Instantiate(nationPrefab[indexList[rand]].gameObject, this.transform.position,rot) ;
			tTemp2.transform.parent = tTemp.transform.GetChild(0);

				if( tTemp2.name =="15_Human_S_5(Clone)") tTemp.GetComponent<NavMeshAgent>().radius = 0.35f;
			if(indexList[rand]>13) tTemp.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled=false;

			randS = Random.Range(0.8f,1.1f);

			tTemp.GetComponent<NavMeshAgent>().speed*=randS;
			if( tTemp2.name !="15_Human_S_5(Clone)") tTemp2.GetComponent<Animator>().speed *=randS;

			tTemp.transform.GetChild(0).GetComponent<AnimForDynamic>().enabled=true;
			//SetAnim();
			//tTemp.transform.GetChild(0).gameObject.SetActive(false);
			//tTemp.transform.GetChild(1).gameObject.SetActive(false);



			indexList.RemoveAt(rand);
		}


		if( this.transform.localPosition.x >-100){
			this.gameObject.SetActive(false);
			navA.enabled = false ;
		}//end
	}//update

	private int currentDes=1;
	void OnTriggerEnter(Collider obj){
		
		if(obj.CompareTag("NavPoint")){

				navA.destination = Nav_Master.GetChild(currentDes+1).GetChild(0).position;
			currentDes +=1;

		}// hit np

	}//triger


	public float[] nationCredit;
	public float[] specialCredit;

	private float totalNation;
	private float totalSpecial;


	private  int[] indexQuery ;

	List<int> indexList = new List<int>();

	private int finalNation;
	private int finalSpecial;

	void TypeQuery (){
		for(int k =0;k<nationCredit.Length;k++){
			totalNation += nationCredit[k];
		}
		for(int k =0;k<specialCredit.Length;k++){
			totalSpecial += specialCredit[k];
		}
		// Find total
			
		finalSpecial =  (int)(charCount*0.28);

		finalNation = charCount-finalSpecial;

		indexQuery = new int[charCount];


		for(int k =0;k<nationCredit.Length;k++){
			nationCredit[k]  = finalNation*(nationCredit[k]/totalNation);
			if(k!=0)
			nationCredit[k] = nationCredit[k-1] + nationCredit[k];
		}
		//-----nation-----

		for(int n =0;n<finalNation;n++){
			for(int l=0;l<nationCredit.Length;l++){
					if(n>nationCredit[l])
					indexQuery[n] = l+1;
			}
//			print(	indexQuery[n]);
		}//nation llop

		//-----speical-----

		for(int k =0;k<specialCredit.Length;k++){
			specialCredit[k]  = finalSpecial*(specialCredit[k]/totalSpecial);
			if(k!=0)
				specialCredit[k] = specialCredit[k-1] + specialCredit[k];

		
		}
		//-----speical-----
		for(int n =0;n<finalSpecial;n++){
		
			for(int l=specialCredit.Length-1;l>-1;l--){

				if(n<specialCredit[l])
					indexQuery[n+finalNation] = l + nationCredit.Length;
			}
		//	print(	indexQuery[n+finalNation]);
		}//nation llop
		
		//-----speical-----

		for(int i =0;i<charCount;i++){
			indexList.Add(indexQuery[i]);
		}//add to List

	}//type query F


	private Animator anim;

	void SetAnim(){
		anim = tTemp2.GetComponent<Animator>();

	
		anim.SetBool("isStatic",false);
		
		
		if(transform.name[9].ToString() == "F"){
			
			anim.SetInteger("animIndex",4);
		}//girl
		else if(transform.name[9].ToString()  == "S"){
			if(transform.name[11].ToString()  == "1")
				anim.SetInteger("animIndex",6);
			else if(transform.name[11].ToString()  == "2")
				anim.SetInteger("animIndex",7);
			else if(transform.name[11].ToString()  == "3")
				anim.SetInteger("animIndex",8);
			else if(transform.name[11].ToString()  == "4")
				anim.SetInteger("animIndex",9);
		}//girl
		else{
			anim.SetInteger("animIndex",(int)Random.Range(1,4));
		}//
	}//

}//class
