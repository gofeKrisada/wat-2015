﻿using UnityEngine;
using System.Collections;

public class Wheel : MonoBehaviour {

	// Use this for initialization
	void Start () {

			if(this.name=="Wheel")
		LeanTween.rotateAroundLocal(this.gameObject,Vector3.right,360,6).setEase(LeanTweenType.linear).setLoopClamp();

			else
		LeanTween.rotateAroundLocal(this.gameObject,Vector3.back,360,6).setEase(LeanTweenType.linear).setLoopClamp();

		this.GetComponent<Wheel>().enabled=false;
	}
	

}
