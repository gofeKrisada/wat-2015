﻿using UnityEngine;
using System.Collections;
//[ExecuteInEditMode]
public class NpcTrigger : MonoBehaviour {



	void Updates(){


		GameObject[] npcExt_Master = GameObject.FindGameObjectsWithTag("NpcExt");
	
		foreach (GameObject go in npcExt_Master) {
			go.GetComponent<AnimControl>().enabled=false;
			for(int j=0;j<go.transform.childCount;j++){
	 

				go.transform.GetChild(j).gameObject.SetActive(false);
						
			}//for
		}//each

	

	}//turn off NPC

	void Start(){
		if(UserData.isTut==0)this.gameObject.SetActive(false);
	}//


	void OnTriggerEnter(Collider obj){
		
		if(obj.CompareTag("NpcExt")){
		//	obj.GetComponent<NavMeshAgent>().enabled=true;
			for(int i=0;i<obj.transform.childCount;i++){
			obj.transform.GetChild(i).gameObject.SetActive(true);
			}//for

			if(obj.GetComponent<AnimControl>()!=null)
			obj.transform.GetComponent<AnimControl>().enabled=true;

		}//
	}//

	void OnTriggerExit(Collider obj){

		if(obj.CompareTag("NpcExt")){
		//	obj.GetComponent<NavMeshAgent>().enabled=false;
			for(int i=0;i<obj.transform.childCount;i++){
				obj.transform.GetChild(i).gameObject.SetActive(false);
			}//for

			if(obj.GetComponent<AnimControl>()!=null)
			obj.gameObject.GetComponent<AnimControl>().enabled=false;
		
		}//

	}//




}//class
