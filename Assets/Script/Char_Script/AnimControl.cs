﻿using UnityEngine;
using System.Collections;

public enum AnimCat{
	stand=1,sit=2,sleep=3
}//cat


public enum StaticStateStand{
	a11_talkStand_1=1,
	a12_talkStand_2=2,
	a13_waitCart=3,
	a14_waitSomeone=4,
	a15_shopKeep_1=5,
	a16_shopKeep_2=6,
	a17_seller=7,
	a18_customer_1=8,
	a19_customer_2=9,
	a110_crowd_1=10,
	a111_crowd_2=11,
	a112_F_idle_1=12,
	a113_F_idle_2=13,
	a114_M_idle=14
}//stat

public enum StaticStateSit{

	b21_queueSit=1,
	b22_layback=2,
	b23_talkSit_1=3,
	b24_talkSit_2=4,
	b25_tea=5,
	b26_shopHigh=6,
	b27_shopLow=7,
	b28_squat=8,
	b29_smoke=9

}//stat

public enum DynamicState{
	b11_normaWalk_1=1,
	b12_normaWalk_2=2,
	b13_snootyWalk=3,
	b14_swaggerWalk=4,
	b15_carry=5,
	b16_shoulder=6,
	b17_bear=7,
	b18_haul=8,
	b19_bike=9
}//dyna


public class AnimControl : MonoBehaviour {

	public bool isStatic;


	public AnimCat animCat;
	public StaticStateStand standAnim;
	public StaticStateSit sitAnim;

	public DynamicState dynamicAnim;

	private int animIndex;
	private Animator anim;




	// Use this for initialization
	void OnEnable () {
		anim =  transform.GetChild(0).GetComponent<Animator>();

		anim.SetBool("isStatic",isStatic);

		if(isStatic){
		anim.SetInteger("animCat",(int)animCat);
			if((int)animCat==1)
		anim.SetInteger("animIndex",(int)standAnim);
			else if((int)animCat==2)
				anim.SetInteger("animIndex",(int)sitAnim);
			else if((int)animCat==3)
				anim.SetInteger("animIndex",1);
		}//isStatic
		else 
			anim.SetInteger("animIndex",(int)dynamicAnim);


	}//start







}//animControl
