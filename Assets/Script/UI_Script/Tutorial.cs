﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Tutorial : MonoBehaviour {

	public GameObject	CanvasTop;

	private Transform canvasTop;
	private Transform player ;
	private RectTransform blackBG;

	private PlayerControl playC;
	private Camera mainCam;
	public Transform talkNpc;

	public GameObject playerView;

	public Transform spwanDes;
	void Awake(){
		//UserData.isTut=0;
		player = GameObject.FindWithTag("Player").transform;

		if(UserData.isTut==1){
			talkNpc.gameObject.SetActive(false);
			playerView.SetActive(true);

			this.GetComponent<MenuVR>().enabled=true;
			this.GetComponent<Tutorial>().enabled=false;

		}//
		else UserData.spawnDestination =3;

		if(UserData.lastScene>=1 )
		player.transform.position = spwanDes.GetChild(UserData.spawnDestination-1).position;

	}//awake

	// Use this for initialization
	void Start () {
		AwakeTut();

		if(UserData.lastScene>=1 && UserData.isTut==0){

		
		player.GetChild(2).GetChild(0).gameObject.SetActive(false);
		mainCam.GetComponent<MouseLook>().enabled=false;
			player.GetComponent<MouseLook>().enabled=false;

			LeanTween.alpha(blackBG,1,0.01f);

				StartCoroutine(DisableLoading(3.5f));
		
			if(UserData.isTut==0){
				LeanTween.rotateLocal(Camera.main.gameObject,new Vector3(0,-25,0),2).setEase(LeanTweenType.easeOutQuart).setDelay(4);
				LeanTween.rotateLocal(Camera.main.gameObject,new Vector3(0,25,0),3).setEase(LeanTweenType.easeOutQuart).setDelay(7);
				StartCoroutine(Tut1_Intro(10.0f));
			}//not tut
			else {
				tutNpc.gameObject.SetActive(false);
				npcMaster.gameObject.SetActive(true);
			}// disappear Tut

		}//past 1st scene

	}//start

	void Update(){

			if(tutDialogCount ==1 && !playC.dialogBox.gameObject.activeSelf ){
			tutDialogCount+=1;
				tut1_hotSpot.gameObject.SetActive(true);
				tutSet[1].gameObject.gameObject.SetActive(true);

				playC.talk_button.gameObject.SetActive(false);
			mainCam.GetComponent<Camera>().enabled=true;
				mainCam.transform.localEulerAngles = new Vector3(0,0,0);
		

			}//end conver 1



		if( playC.tutTalk ==1 ){
			playC.tutTalk=2;

			tut1_hotSpot.GetChild(0).gameObject.SetActive(false);
			tut1_hotSpot.GetChild(1).gameObject.SetActive(false);

			tutSet[1].gameObject.gameObject.SetActive(false);
			tutSet[2].gameObject.gameObject.SetActive(true);

			playC.canControl.SetActive(false);
mainCam.GetComponent<MouseLook>().enabled=true;
			player.GetComponent<MouseLook>().enabled=true;
		}//tut hit hotpsot

		else if( playC.tutTalk ==2 ){

			playC.tutTalk=3;
			tutView_2.gameObject.SetActive(true);
	
			playC.talkNPC.GetComponent<NpcDialog>().dialogEN = new string[1]{"เสร็จสิ้นการแนะนำ ขอให้เพลิดเพลินกับการท่องเที่ยวนะเจ้า"};
			playC.talkNPC.GetComponent<NpcDialog>().dialogCN = new string[1]{"这所房子的架构是拉娜和西式的结合"};
			if(UserData.lang==1)
			playC.talkNPC.GetComponent<NpcDialog>().dialogSet =playC.talkNPC.GetComponent<NpcDialog>().dialogEN;
			else 
				playC.talkNPC.GetComponent<NpcDialog>().dialogSet =playC.talkNPC.GetComponent<NpcDialog>().dialogCN;
		}//2

		else if( playC.tutTalk ==3 ){
				if(!tutView_2.gameObject.activeSelf){
				tutSet[2].gameObject.gameObject.SetActive(false);
				tutSet[3].gameObject.gameObject.SetActive(true);

				playC.tutTalk=4;
					Destroy(tutView_2.gameObject);
				tut1_hotSpot.GetChild(3).gameObject.SetActive(false);
				tut1_hotSpot.GetChild(4).gameObject.SetActive(true);

				playC.canControl.SetActive(true);

		
				}// hit npc
		}//tut 3
		else if(playC.tutTalk==4){
			if(playC.dialogBox.gameObject.activeSelf){
				playC.tutTalk=5;
				tutSet[3].gameObject.gameObject.SetActive(false);
			}
		}//tut final

		else if(playC.tutTalk==5){
			if(!playC.dialogBox.gameObject.activeSelf){
				playC.tutTalk=6;
				StartCoroutine(Tut4_Final(0.5f));
			}
		}//tut final

	}// update
	


	IEnumerator DisableLoading(float time) {

		yield return new WaitForSeconds(time);
		if(UserData.isTut==1){
		player.GetChild(2).GetChild(0).gameObject.SetActive(true);
			mainCam.GetComponent<MouseLook>().enabled=true;
			player.GetComponent<MouseLook>().enabled=true;
		}//not tut

		canvasTop.GetChild(1).gameObject.SetActive(false);
	
		LeanTween.alpha(blackBG,0,1f);
	}//disLoading


	void SetUpTut(){
		playC = GameObject.FindWithTag("Player").GetComponent<PlayerControl>();
		playC.talkNPC = talkNpc;
		playC.talk_button.gameObject.SetActive(false);
	}//set up tut

	public Transform tutNpc;
	public Transform npcMaster;

	public Transform tut1_hotSpot;
	public Transform[] tutSet;

	public Transform tutView_2;


	private int tutDialogCount;
	IEnumerator Tut_DialogCount(float time) {
		
		yield return new WaitForSeconds(time);
		tutDialogCount +=1;
		playC.talk_button.gameObject.SetActive(false);

		
	}//disLoading
		
	IEnumerator Tut1_Intro(float time) {
		
		yield return new WaitForSeconds(time);
		StartCoroutine(Tut_DialogCount(2.5f));
		playC.ShowDialog();

	}//disLoading

	IEnumerator Tut4_Final(float time) {

		playC.canControl.SetActive(false);
		mainCam.GetComponent<MouseLook>().enabled=false;
		player.GetComponent<MouseLook>().enabled=false;

		yield return new WaitForSeconds(time);

	
		UserData.isTut=1;
		UserData.spawnDestination=3;
		LeanTween.alpha(blackBG,1,0.01f);

		StartCoroutine(LoadAsync("Scene_VR"));
	}//disLoading

	private GameObject loadingSet;

	private IEnumerator LoadAsync(string levelName)
	{
		
		loadingSet.SetActive(true);
		LeanTween.rotateAround(loadingSet.transform.GetChild(1).GetComponent<RectTransform>(),Vector3.forward,360,6).setEase(LeanTweenType.linear).setLoopClamp();
		
		
		AsyncOperation operation = Application.LoadLevelAsync(levelName);
		
		while(!operation.isDone) {
			yield return operation.isDone;
//			Debug.Log("loading progress: " + operation.progress);
		}
	//	Debug.Log("load done");
	}//loadAsync 

	void AwakeTut(){
		mainCam = Camera.main.GetComponent<Camera>();
		
	
		
		
		
		SetUpTut();
		
		if(GameObject.FindWithTag("CanvasTop") !=null)
			canvasTop = GameObject.FindWithTag("CanvasTop").transform;
		else 
			Instantiate( CanvasTop);
		
		
		loadingSet =   GameObject.FindWithTag("CanvasTop").transform.GetChild(1).gameObject;
		
		blackBG = GameObject.FindWithTag("CanvasTop").transform.GetChild(0).GetComponent<RectTransform>();
		
	
	}//awakTut

}//class
