﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Preload : MonoBehaviour {

	public RectTransform black;

	public GameObject ld;

	// Use this for initialization
	void Start () {
		LeanTween.alpha(black,0,0.50f);

		LeanTween.alpha(black,1,0.50f).setDelay(2.5f);

		LeanTween.alpha(black,0,0.50f).setDelay(3.0f);


		StartCoroutine(LoadBG (3.0f));

		StartCoroutine(LoadScene(4));
	}//start

	IEnumerator LoadScene(float time){

		yield return new WaitForSeconds(time);

		Application.LoadLevel(1);

	}//loadscene

	IEnumerator LoadBG(float time){

		yield return new WaitForSeconds(time);
		
		ld.SetActive(true);
		
	}//loadscene



}//class
