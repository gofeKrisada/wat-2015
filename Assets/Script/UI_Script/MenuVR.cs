﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class MenuVR : MonoBehaviour {

	public GameObject	CanvasTop;

	private Transform canvasTop;
	private Transform player ;
	private RectTransform blackBG;


	private Camera mainCam;


	private EventSystem e;
	// Use this for initialization
	void Start () {
		//e = EventSystem.current.gameObject.GetComponent<StandaloneInputModule>().


		mainCam = Camera.main.GetComponent<Camera>();
		
		player = GameObject.FindWithTag("Player").transform;
		

		
		if(GameObject.FindWithTag("CanvasTop") !=null)
			canvasTop = GameObject.FindWithTag("CanvasTop").transform;
		else 
			Instantiate( CanvasTop);
		
		
		loadingSet =   GameObject.FindWithTag("CanvasTop").transform.GetChild(1).gameObject;
		
		blackBG = GameObject.FindWithTag("CanvasTop").transform.GetChild(0).GetComponent<RectTransform>();



	

		
		player.GetChild(2).GetChild(0).gameObject.SetActive(false);
		mainCam.GetComponent<MouseLook>().enabled=false;
			player.GetComponent<MouseLook>().enabled=false;

			LeanTween.alpha(blackBG,1,0.01f);

		if(UserData.lastScene==0)
				StartCoroutine(DisableLoading(0.5f));
		else 
			StartCoroutine(DisableLoading(3.5f));


	

		menuButton.gameObject.SetActive(true);
	}//start


	
	public GameObject menuButton;

	IEnumerator DisableLoading(float time) {

		yield return new WaitForSeconds(time);
		if(UserData.isTut==1){
				
			player.GetChild(2).GetChild(0).gameObject.SetActive(true);
			mainCam.GetComponent<MouseLook>().enabled=true;
			player.GetComponent<MouseLook>().enabled=true;

	
		}//not tut

		if(UserData.lastScene>=1) canvasTop.GetChild(1).gameObject.SetActive(false);
	
		LeanTween.alpha(blackBG,0,1f);
	}//disLoading



	private GameObject loadingSet;

	private IEnumerator LoadAsync(string levelName)
	{
		
		loadingSet.SetActive(true);
		LeanTween.rotateAround(loadingSet.transform.GetChild(1).GetComponent<RectTransform>(),Vector3.forward,360,6).setEase(LeanTweenType.linear).setLoopClamp();
		
		
		AsyncOperation operation = Application.LoadLevelAsync(levelName);
		
		while(!operation.isDone) {
			yield return operation.isDone;
			Debug.Log("loading progress: " + operation.progress);
		}
		Debug.Log("load done");
	}//loadAsync 

	public RectTransform leftDpad;
	public RectTransform rightDpad;

	public  void DragScreenLeft(){

		#if UNITY_EDITOR
		leftDpad.position = Input.mousePosition;
		#else 
		leftDpad.position = Input.GetTouch(leftT-1).position;
		#endif
	}//pressLeft

	public  void DragScreenRight(){
		#if UNITY_EDITOR
		rightDpad.position = Input.mousePosition;
		#else 
		rightDpad.position = Input.GetTouch(rightT-1).position;
		#endif
	

	}//pressRight

	private int leftT;
	private int rightT;
	public  void PressScreenLeft(){
		#if UNITY_EDITOR

		#else 
		leftT = Input.touchCount;
		leftDpad.position = Input.GetTouch(leftT-1).position;
		#endif

		leftDpad.gameObject.SetActive(true);
	}//pressLeft

	public  void PressScreenRight(){

		#if UNITY_EDITOR
		
		#else 
		rightT = Input.touchCount;
		rightDpad.position = Input.GetTouch(rightT-1).position;
		#endif

		rightDpad.gameObject.SetActive(true);
	}//pressRight

	public  void UpScreenLeft(){
		leftDpad.gameObject.SetActive(false);

	}//upLeft
	
	public  void UpScreenRight(){
		rightDpad.gameObject.SetActive(false);
	}//upRight

	public GameObject mapMenu;
	public void ToggleMap(){
		AudioPlayer.Play(AudioPlayer.sfx_button1);
		mapMenu.SetActive(!mapMenu.activeSelf);

		player.GetChild(2).GetChild(0).gameObject.SetActive( !mapMenu.activeSelf);
		mainCam.GetComponent<MouseLook>().enabled= !mapMenu.activeSelf;
		player.GetComponent<MouseLook>().enabled= !mapMenu.activeSelf;

		mainCam.enabled = !mapMenu.activeSelf;
	}//open map

	public GameObject placeName;
	public void TogglePlaceName(){
		AudioPlayer.Play(AudioPlayer.sfx_button1);
		placeName.SetActive(!placeName.activeSelf);
	}//open map

	public GameObject confimExit;
	public void ToggleExitConfirm(){
		AudioPlayer.Play(AudioPlayer.sfx_button1);
		confimExit.SetActive(!confimExit.activeSelf);
	
	}//open map



}//class
