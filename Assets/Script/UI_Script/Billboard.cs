﻿using UnityEngine;


public class Billboard : MonoBehaviour
{
	public Camera cam;

	void Start(){
		cam = Camera.main;
	}//start

	void FixedUpdate() 
	{
		transform.LookAt(cam.transform.position, Vector3.up);
	}//fixed	  Update 

}//class