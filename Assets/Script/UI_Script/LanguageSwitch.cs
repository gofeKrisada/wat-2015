﻿using UnityEngine;
using System.Collections;

public class LanguageSwitch : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.GetChild(0).gameObject.SetActive(false);
		transform.GetChild(1).gameObject.SetActive(false);


		transform.GetChild(UserData.lang-1).gameObject.SetActive(true);

		this.GetComponent<LanguageSwitch>().enabled=false;
	}//
	

}//
