﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tut_HandPointer : MonoBehaviour {
	private RectTransform tr;
	public Transform tutArrow;
	// Use this for initialization
void OnEnable(){

		LeanTween.moveLocalY(tutArrow.gameObject,0.7f,1.0f).setEase(LeanTweenType.easeInOutSine).setLoopPingPong();;


		tr = this.GetComponent<RectTransform>();

		tr.localPosition = new Vector3(0,-200,0);
		LeanTween.scale(tr.gameObject,Vector3.one,0.5f).setEase(LeanTweenType.easeOutQuart);
		LeanTween.moveLocalY(tr.gameObject,200,1.5f).setEase(LeanTweenType.easeOutQuart).setDelay(0.5f);
		LeanTween.scale(tr.gameObject,Vector3.one*2,0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(2f);
		LeanTween.moveLocal(tr.gameObject,new Vector3(0,200,0),0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(2.0f);
		
		LeanTween.scale(tr.gameObject,Vector3.one,0.5f).setEase(LeanTweenType.easeOutQuart).setDelay(2.1f);
		LeanTween.moveLocalY(tr.gameObject,-200,1.5f).setEase(LeanTweenType.easeOutQuart).setDelay(2.6f);
		LeanTween.scale(tr.gameObject,Vector3.one*2,0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(4.1f);
		LeanTween.moveLocal(tr.gameObject,new Vector3(-200,0,0),0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(4.1f);
		
		LeanTween.scale(tr.gameObject,Vector3.one,0.5f).setEase(LeanTweenType.easeOutQuart).setDelay(4.2f);
		LeanTween.moveLocalX(tr.gameObject,200,1.5f).setEase(LeanTweenType.easeOutQuart).setDelay(4.7f);
		LeanTween.scale(tr.gameObject,Vector3.one*2,0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(6.2f);
		LeanTween.moveLocal(tr.gameObject,new Vector3(200,0,0),0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(6.2f);
		
		LeanTween.scale(tr.gameObject,Vector3.one,0.5f).setEase(LeanTweenType.easeOutQuart).setDelay(6.3f);
		LeanTween.moveLocalX(tr.gameObject,-200,1.5f).setEase(LeanTweenType.easeOutQuart).setDelay(6.8f);
		LeanTween.scale(tr.gameObject,Vector3.one*2,0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(8.3f);
		StartCoroutine(MovingLoop(8.4f));
	}//On enable
	
	IEnumerator MovingLoop(float time) {
		
		yield return new WaitForSeconds(time);
		
		tr.localPosition = new Vector3(0,-200,0);
		LeanTween.scale(tr.gameObject,Vector3.one,0.5f).setEase(LeanTweenType.easeOutQuart);
		LeanTween.moveLocalY(tr.gameObject,200,1.5f).setEase(LeanTweenType.easeOutQuart).setDelay(0.5f);
		LeanTween.scale(tr.gameObject,Vector3.one*2,0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(2f);
		LeanTween.moveLocal(tr.gameObject,new Vector3(0,200,0),0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(2.0f);
		
		LeanTween.scale(tr.gameObject,Vector3.one,0.5f).setEase(LeanTweenType.easeOutQuart).setDelay(2.1f);
		LeanTween.moveLocalY(tr.gameObject,-200,1.5f).setEase(LeanTweenType.easeOutQuart).setDelay(2.6f);
		LeanTween.scale(tr.gameObject,Vector3.one*2,0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(4.1f);
		LeanTween.moveLocal(tr.gameObject,new Vector3(-200,0,0),0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(4.1f);
		
		LeanTween.scale(tr.gameObject,Vector3.one,0.5f).setEase(LeanTweenType.easeOutQuart).setDelay(4.2f);
		LeanTween.moveLocalX(tr.gameObject,200,1.5f).setEase(LeanTweenType.easeOutQuart).setDelay(4.7f);
		LeanTween.scale(tr.gameObject,Vector3.one*2,0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(6.2f);
		LeanTween.moveLocal(tr.gameObject,new Vector3(200,0,0),0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(6.2f);
		
		LeanTween.scale(tr.gameObject,Vector3.one,0.5f).setEase(LeanTweenType.easeOutQuart).setDelay(6.3f);
		LeanTween.moveLocalX(tr.gameObject,-200,1.5f).setEase(LeanTweenType.easeOutQuart).setDelay(6.8f);
		LeanTween.scale(tr.gameObject,Vector3.one*2,0.01f).setEase(LeanTweenType.easeOutQuart).setDelay(8.3f);
		StartCoroutine(MovingLoop(8.4f));
	}//movingLoop
}//class
