﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuHome : MonoBehaviour {
	public GameObject CanvasTop;

	private RectTransform black;
	
	private GameObject loadingSet;

	void Awake(){
		if(GameObject.FindWithTag("CanvasTop") !=null){
			CanvasTop = GameObject.FindWithTag("CanvasTop");
		black=CanvasTop.transform.GetChild(0).GetComponent<RectTransform>();
		loadingSet = CanvasTop.transform.GetChild(1).gameObject;

			LeanTween.alpha(black,1,1f);
			LeanTween.alpha(black,0,1f).setDelay(1);

			StartCoroutine(DisableLoading(1f));
		}//has 
		else {
			CanvasTop =  Instantiate( CanvasTop);
			CanvasTop.name = "[Canvas_Top]";
			black=CanvasTop.transform.GetChild(0).GetComponent<RectTransform>();
			loadingSet = CanvasTop.transform.GetChild(1).gameObject;
			DontDestroyOnLoad(CanvasTop);

			
			LeanTween.alpha(black,1,0f);
			//-----------------
			LeanTween.alpha(black,0,1f);
		}//
	}//awake

	// Use this for initialization
	void Start () {
		UserData.lastScene+=1;


		LeanTween.moveLocalY(popNormal,popNormal.GetComponent<RectTransform>().localPosition.y+4,1.0f).setEase(LeanTweenType.easeInOutSine).setLoopPingPong();
	}//start



	public void ButtonStart(){
		AudioPlayer.Play(AudioPlayer.sfx_button1);
		
		//popNormal.SetActive(false);
		if(!popAR.activeSelf){
			if(!popVAR.activeSelf){
				
			}//false
			else {
				StartCoroutine(popDis(0.5f,popVAR));
				LeanTween.scale(popVAR.transform.GetChild(0).GetComponent<RectTransform>(),Vector3.zero*0.8f,1f).setEase(LeanTweenType.easeOutQuart);
			}//true

			popAR.SetActive(true);
			
			for(int i=0;i<3;i++){
				LeanTween.scale(popAR.transform.GetChild(i).GetComponent<RectTransform>(),Vector3.one*0.8f,1f).setEase(LeanTweenType.easeOutExpo);
				LeanTween.moveLocalY(popAR.transform.GetChild(i).gameObject,popAR.transform.GetChild(i).GetComponent<RectTransform>().localPosition.y+6,0.75f).setEase(LeanTweenType.easeInOutSine).setLoopPingPong().setDelay(0.15f);
				
			}//for
		}//false
		else {
			StartCoroutine(popDis(0.5f,popAR));
			for(int i=0;i<3;i++)
				LeanTween.scale(popAR.transform.GetChild(i).GetComponent<RectTransform>(),Vector3.zero*0.8f,1f).setEase(LeanTweenType.easeOutQuart);
		}//true

	
	}//start

	public void ButtonVR_Start(int des){
		
		AudioPlayer.Play(AudioPlayer.sfx_button1);
		LeanTween.rotateAround(loadingSet.transform.GetChild(1).GetComponent<RectTransform>(),Vector3.forward,360,6).setEase(LeanTweenType.linear).setLoopClamp();
		UserData.spawnDestination = des;
		StartCoroutine(LoadAsync("Scene_VR"));
	}//start btn

	public GameObject popNormal;
	public GameObject popAR;

	public GameObject popVAR;
	public void ButtonAR(){
		if(!popVAR.activeSelf){
			//popNormal.SetActive(false);
			if(!popAR.activeSelf){
			}//false
			else {
				StartCoroutine(popDis(0.5f,popAR));
				for(int i=0;i<3;i++)
					LeanTween.scale(popAR.transform.GetChild(i).GetComponent<RectTransform>(),Vector3.zero*0.8f,1f).setEase(LeanTweenType.easeOutQuart);
			}//true


			popVAR.SetActive(true);

				LeanTween.scale(popVAR.transform.GetChild(0).GetComponent<RectTransform>(),Vector3.one*0.8f,1f).setEase(LeanTweenType.easeOutExpo);
				LeanTween.moveLocalY(popVAR.transform.GetChild(0).gameObject,popVAR.transform.GetChild(0).GetComponent<RectTransform>().localPosition.y+6,0.75f).setEase(LeanTweenType.easeInOutSine).setLoopPingPong().setDelay(0.15f);

		}//false
		else {
			StartCoroutine(popDis(0.5f,popVAR));
				LeanTween.scale(popVAR.transform.GetChild(0).GetComponent<RectTransform>(),Vector3.zero*0.8f,1f).setEase(LeanTweenType.easeOutQuart);
		}//true
	}//start btn

	public void ButtonAR_Start(){
		
		AudioPlayer.Play(AudioPlayer.sfx_button1);
		LeanTween.rotateAround(loadingSet.transform.GetChild(1).GetComponent<RectTransform>(),Vector3.forward,360,6).setEase(LeanTweenType.linear).setLoopClamp();
		StartCoroutine(LoadAsync("Scene_AR"));
	}//start btn




	IEnumerator popDis(float time, GameObject pop) {
		
		yield return new WaitForSeconds(time);
		pop.SetActive(false);
	}//disLoading

	public void ButtonStartAR(int num){
		AudioPlayer.Play(AudioPlayer.sfx_button1);
		StartCoroutine(LoadAsync("Scene_AR_"+num.ToString()));
	}//start



	public GameObject btnEN;
	public GameObject btnCN;

	public void ButtonEN(){
		AudioPlayer.Play(AudioPlayer.sfx_button1);

		btnEN.SetActive(false);
		btnCN.SetActive(true);
		UserData.lang = 2;
		UserData.isTut=0;
	}//EN

	public void ButtonCN(){
		AudioPlayer.Play(AudioPlayer.sfx_button1);

		btnEN.SetActive(true);
		btnCN.SetActive(false);
		UserData.lang = 1;

		UserData.isTut=0;
	}//CN



	private IEnumerator LoadAsync(string levelName)
	{

		loadingSet.SetActive(true);
	//	LeanTween.rotateAround(loadingSet.transform.GetChild(1).GetComponent<RectTransform>(),Vector3.forward,360,6).setEase(LeanTweenType.linear).setLoopClamp();


		AsyncOperation operation = Application.LoadLevelAsync(levelName);

		while(!operation.isDone) {
			yield return operation.isDone;
//			Debug.Log("loading progress: " + operation.progress);
		}
	//	Debug.Log("load done");
	}//loadAsync 

	IEnumerator DisableLoading(float time) {
		
		yield return new WaitForSeconds(time);

		CanvasTop.transform.GetChild(1).gameObject.SetActive(false);

	}//disLoading

}//class
