﻿using UnityEngine;
using System.Collections;

public class RotateAround : MonoBehaviour {

	public bool isUI;
	public float speed =6;
	// Use this for initialization
	void Start () {
		if(isUI)
		LeanTween.rotateAround(this.GetComponent<RectTransform>(),Vector3.forward,360,speed).setEase(LeanTweenType.linear).setLoopClamp();
	
	}
	

}//c'ass
