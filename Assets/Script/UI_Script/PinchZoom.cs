﻿using UnityEngine;
using UnityEngine.UI;

public class PinchZoom : MonoBehaviour
{

	private  float orthoZoomSpeed = 0.04f;        // The rate of change of the orthographic size in orthographic mode.

	public RectTransform map;

	private float scaleS = 1;

	private float deltaMagnitudeDiff ;
	float touchDeltaMag;
	float prevTouchDeltaMag;
	Vector2 touchZeroPrevPos;
	Vector2 touchOnePrevPos;
	Touch touchZero;
		Touch touchOne;

	public Transform placeName;

	public ScrollRect scrollR;

	void Start(){
		player = GameObject.FindWithTag("Player").transform;
	}//start

	void Update()
	{
		if( placeName.gameObject.activeSelf && placeName.GetChild(0).localScale.x !=( 1-((scaleS-1) /4)))
			for(int i=0;i<placeName.childCount;i++)
				placeName.GetChild(i).localScale = Vector3.one*( 1-((scaleS-1) /4));



		// If there are two touches on the device...
		if (Input.touchCount == 2)
		{
			Pinching();

		}//count
		else if(Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.X)){
		
			
			// ... change the orthographic size based on the change in distance between the touches.
			if(Input.GetKey(KeyCode.Z))	scaleS += Time.deltaTime *3;
			else if(Input.GetKey(KeyCode.X)) scaleS -= Time.deltaTime *3;
		
			// Make sure the orthographic size never drops below zero.
		//	scaleS = Mathf.Max(scaleS, 5f);
			scaleS = Mathf.Max(scaleS, 1f);
			scaleS = Mathf.Min(scaleS, 3f);
			
			map.localScale = Vector3.one*scaleS;

			if( placeName.gameObject.activeSelf)
			for(int i=0;i<placeName.childCount;i++)
				placeName.GetChild(i).localScale = Vector3.one*( 1-((scaleS-1) /4));
		}//key Zoom

		GPS();
	}//update

void Pinching(){
		// Store both touches.
		touchZero = Input.GetTouch(0);
		touchOne = Input.GetTouch(1);
		
		// Find the position in the previous frame of each touch.
		touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
		
		// Find the magnitude of the vector (the distance) between the touches in each frame.
		prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
		
		// Find the difference in the distances between each frame.
		deltaMagnitudeDiff	= prevTouchDeltaMag - touchDeltaMag;
		
		
		// ... change the orthographic size based on the change in distance between the touches.
		scaleS -= deltaMagnitudeDiff * orthoZoomSpeed;
		
		// Make sure the orthographic size never drops below zero.
		scaleS = Mathf.Max(scaleS, 1f);
		scaleS = Mathf.Min(scaleS, 3f);
		
		map.localScale = Vector3.one*scaleS;
		
		if( placeName.gameObject.activeSelf)
			for(int i=0;i<placeName.childCount;i++)
				placeName.GetChild(i).localScale = Vector3.one*( 1-((scaleS-1) /4));
	}//pinching

	private Vector2 mapCorX= new Vector2 (-230,867);
	private Vector2 mapCorY= new Vector2 (-165,250);
	private Vector2 realCorX= new Vector2 (-278.4f,-122.5f);
	private Vector2 realCorY= new Vector2 (-18.7f,17.4f);

	private Vector2   postionScale;

	private Transform player;

	public RectTransform gps;
	void GPS(){
			
		postionScale.x =  ( player.transform.position.x - realCorX.x) / (realCorX.y-realCorX.x);
		postionScale.y =  (player.transform.position.z -realCorY.x)/ (realCorY.y-realCorY.x);



		gps.localPosition = new Vector3( mapCorX.x+	(postionScale.x  * (mapCorX.y-mapCorX.x)),
		                                mapCorY.x+	(postionScale.y  * (mapCorY.y-mapCorY.x)),
		                               							 gps.localPosition.z);

	}//gps



}//class